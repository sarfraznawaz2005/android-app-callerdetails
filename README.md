### What's CallerDetails? ###

CallerDetails is an Android app that shows contact details saved against a contact such as organization, email, address, note, etc in a toast message whenever there is an incoming call. If you have not set these fields for a contact, toast will not be shown.

In order to add contact details go to mobile Contacts and add contact fields such as Organization Nickname Email etc.